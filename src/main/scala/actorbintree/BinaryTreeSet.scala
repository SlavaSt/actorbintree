/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue
import actorbintree.BinaryTreeNode.{CopyFinished, CopyTo}

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef

    def id: Int

    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation


  /** Request to perform garbage collection */
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {

  import BinaryTreeSet._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case message =>
      println("BinaryTreeSet")
      message match {
        case Insert(requester, id, elem) => println(s"Insert($requester, $id, $elem"); root ! Insert(requester, id, elem)
        case Contains(requester, id, elem) => println(s"Contains($requester, $id, $elem"); root ! Contains(requester, id, elem)
        case Remove(requester, id, elem) => println(s"Remove($requester, $id, $elem"); root ! Remove(requester, id, elem)
        case GC =>
          val newRoot = createRoot
          root ! CopyTo(newRoot)
          context.become(garbageCollecting(newRoot))
      }
  }


  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case oper: Operation => pendingQueue = pendingQueue :+ oper
    case GC => // Ignore new GC request while doing GC
    case CopyFinished =>
      context.become(normal)
      root ! PoisonPill
      root = newRoot
      drainQueue()
  }

  final def drainQueue() = {
    pendingQueue.foreach(root ! _)
    pendingQueue = Queue.empty[Operation]
  }

}

object BinaryTreeNode {

  trait Position

  case object Left extends Position

  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)

  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with ActorLogging {

  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case Insert(requester, id, el) =>
      println("Node " + this.elem + " " + removed)
      println("insert " + " " + id + " " + el)
      println(self)
      def ack() = requester ! OperationFinished(id)
      def insert(node: ActorRef) = node ! Insert(requester, id, el)

      if (el == this.elem) {
        removed = false
        ack
      }
      else if (el < this.elem) {
        subtrees.get(Left) match {
          case Some(node) => insert(node)
          case _ =>
            subtrees += (Left -> newNode(el))
            ack
        }
      } else {
        subtrees.get(Right) match {
          case Some(node) => insert(node)
          case _ =>
            subtrees += (Right -> newNode(el))
            ack
        }
      }

    case Contains(requester, id, el) =>
      def contains(node: ActorRef) = node ! Contains(requester, id, el)
      def ack(result: Boolean) = requester ! ContainsResult(id, result)

      if (el == this.elem) {
        ack(!removed)
      } else
      if (el < this.elem) {
        subtrees.get(Left) match {
          case Some(node) => contains(node)
          case _ => ack(result = false)
        }
      } else {
        subtrees.get(Right) match {
          case Some(node) => contains(node)
          case _ => ack(result = false)
        }
      }

    case Remove(requester, id, el) =>
      def remove(node: ActorRef) = node ! Remove(requester, id, el)
      def ack() = requester ! OperationFinished(id)

      if (el == this.elem) {
        removed = true
        ack
      } else
      if (el < this.elem) {
        subtrees.get(Left) match {
          case Some(node) => remove(node)
          case _ => ack
        }
      } else {
        subtrees.get(Right) match {
          case Some(node) => remove(node)
          case _ => ack
        }
      }

    case CopyTo(root) =>
      if (!removed || subtrees.nonEmpty) {
        context.become(copying(subtrees.values.toSet, removed))
        if (!removed) {
          root ! Insert(self, -1, elem)
        }
        subtrees.values.foreach {
          _ ! CopyTo(root)
        }
      }
      else shutdown()
  }

  def newNode(elem: Int) = context.actorOf(BinaryTreeNode.props(elem, initiallyRemoved = false))

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(-1) =>
      if (expected.isEmpty) shutdown()
      else context.become(copying(expected, insertConfirmed = true))
    case CopyFinished =>
      val newExpected = expected - sender
      if (newExpected.isEmpty && insertConfirmed) {
        shutdown()
      } else {
        context.become(copying(newExpected, insertConfirmed))
      }
  }

  def shutdown() = {
    context.parent ! CopyFinished
    //context.stop(self) // TODO Dangerous, some operations may still be "flying"
  }

}
